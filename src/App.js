import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import PricingButton from './PricingButton';

class App extends Component {
  render() {
    return (
      <div className="App">
        <PricingButton />
      </div>
    );
  }
}

export default App;
