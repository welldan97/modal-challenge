import React, { Component } from 'react';

import withModal from './withModal';
const PricingModal = ({ title, onHide }) => (
  <button onClick={onHide}>Hide {title} modal</button>
);

class PricingButton extends Component {
  showPricing = () => {
    const { openModal, hideModal } = this.props;
    openModal('pricing', {
      title: 'Pricing',
      onHide: data => hideModal('pricing'),
    });
  };

  render() {
    return <button onClick={this.showPricing}>Open modal</button>;
  }
}

export default withModal('pricing', PricingModal)(PricingButton);
