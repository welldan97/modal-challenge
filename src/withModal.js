import React, { Component } from 'react';
import ReactDOM from 'react-dom';

function getDisplayName(Component) {
  return Component.displayName || Component.name || 'Component';
}

const withModal = (modalName, ModalComponent) => {
  const handleOpenModal = (modalName, modalProps) => {
    ReactDOM.render(
      <ModalComponent {...modalProps}>hi</ModalComponent>,
      document.getElementById(modalName),
    );
  };

  // Why we need modal name here??
  const handleHideModal = modalName => {
    ReactDOM.unmountComponentAtNode(document.getElementById(modalName));
  };

  return BaseComponent =>
    class WithModalBase extends Component {
      static displayName = `WithModalButtonBase(${getDisplayName(
        BaseComponent,
      )})`;

      render() {
        return (
          <div>
            <BaseComponent
              openModal={handleOpenModal}
              hideModal={handleHideModal}
            />
          </div>
        );
      }
    };
};

export default withModal;
