import React, { Component } from 'react';
import ReactDOM from 'react-dom';

function getDisplayName(Component) {
  return Component.displayName || Component.name || 'Component';
}

class Modal extends Component {
  constructor(props) {
    super(props);
    const { id } = this.props;
    this.el = document.createElement('div');
    this.root = document.getElementById(id);
  }

  componentDidMount() {
    this.root.appendChild(this.el);
  }

  componentWillUnmount() {
    this.root.removeChild(this.el);
  }

  render() {
    return ReactDOM.createPortal(this.props.children, this.el);
  }
}

const withModal = (modalName, ModalComponent) => {
  return BaseComponent =>
    class WithModalBase extends Component {
      constructor(props) {
        super(props);
        this.state = {
          modalProps: undefined,
          isModalShown: false,
        };
      }

      static displayName = `WithModalButtonBase(${getDisplayName(
        BaseComponent,
      )})`;

      handleOpenModal = (modalName, modalProps) => {
        this.setState(() => ({
          modalProps,
          isModalShown: true,
        }));
      };

      handleHideModal = modalName => {
        this.setState(() => ({
          isModalShown: false,
        }));
      };

      render() {
        const { modalProps, isModalShown } = this.state;
        return (
          <div>
            {isModalShown && (
              <Modal id={modalName}>
                <ModalComponent {...modalProps} />
              </Modal>
            )}
            <BaseComponent
              openModal={this.handleOpenModal}
              hideModal={this.handleHideModal}
            />
          </div>
        );
      }
    };
};

export default withModal;
